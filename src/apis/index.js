var apibaseurl = "https://api.sgbregistration.in/api/"
import Vue from 'vue'
import VueResource from 'vue-resource';
//import store from '../store'

export default{

    getStates(){
        return Vue.http.get(apibaseurl + 'Masters/ReadAllState');
    },

    getDistricts(stateid){
        return Vue.http.get(apibaseurl + 'Masters/ReadDistrictByStateID/'+stateid);
    },

    getTaluka(districtid){
        return Vue.http.get(apibaseurl + 'Masters/ReadTalukaByDistID/'+ districtid);
    },

}