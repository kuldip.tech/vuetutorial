import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isLoggedIn: false,
        userName: ""
    },
    mutations: {
        LOGIN_SUCCESS(state,userName) {
            state.isLoggedIn = true;
            state.userName = userName;
        },
        LOGOUT: function (state) {
            
            state.isLoggedIn = false;
            state.userName = "";
        },
    },
    actions: {
        login: function ({ commit }, userName) {
            commit("LOGIN_SUCCESS", userName);
        },
        logout: function ({ commit }) {
            commit("LOGOUT");
        },
    },
    getters:{
        checkLogin:function(state){
            return state.isLoggedIn;
        },
        getUserName: function(state){
            return state.userName;
        }
    }
    // modules: {
    // }
})